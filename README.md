# sky_lab_test-case

## Тестовое задание:
Отображения погоды в текущем городе. Приложение должно выполнять следующие функции:
- Первоначально получать информацию о погоде в текущем местоположении пользователя.
- Сохранять данные о погоде в локальную БД.
- Обновлять погоду при каждом запуске приложения (и при разворачивании из фона)
- Должна быть возможность обновить данные о погоде принудительно
- Предоставлять пользователю возможность просматривать данные о предыдущих измерениях из локальной БД в виде списка. Приложение должно работать на iPhone и iPad, версия iOS>=10. Все внештатные ситуации должны быть корректно обработаны. API сервиса информации о погоде - на усмотрение разработчика. Например: https://openweathermap.org/current
